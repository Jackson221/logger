# Logger Module

The logger module provides easy logging capabilities using Module3D's custom printf function.

# Dependancies

Depends upon module3D's string_manip module

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
