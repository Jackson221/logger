//Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "logger.hpp"

namespace logger
{


/*
#define internal_make_initializer(type)\
	struct _init_for_ ##type \
	{\
		_init_for_ ##type () \
		{\
			flush_log(#type ".log");\
		}\
	} _my_init_for_ ##type ;

	internal_make_initializer(error)
	internal_make_initializer(console)
	internal_make_initializer(vulkan)
	internal_make_initializer(unit_test)
*/
	void init()
	{
		flush_log("error.log");
		flush_log("console.log");
		flush_log("vulkan.log");
		flush_log("unit_test.log");
	}

}
