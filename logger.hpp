//Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "filesystem/filesystem.hpp"
#include "string_manip/sprintf.hpp"
#pragma once 

#ifdef PLATFORM_ANDROID
	#include <android/log.h>
#endif

namespace logger
{
	inline const std::string get_log_path(const char* name)
	{
		return filesystem::concat_directories(filesystem::app_root_directory,"log",name);
	}

	template<const char* logfile, const char* formatstr, typename ...T >
	void log(T ... args)
	{
		std::string buffer = "";//string_manip::sprintf<formatstr>(args...) + "\n";
#ifdef PLATFORM_ANDROID 
		__android_log_print(ANDROID_LOG_INFO, logfile, "%s", buffer.c_str());
#else
		filesystem::append_to_file(get_log_path(logfile),buffer);
#endif
	}

	inline void flush_log(const char* name)
	{
#ifdef PLATFORM_ANDROID 
		//No way to flush logs on android.
#else
		filesystem::write_to_file(get_log_path(name),"");
#endif

	
	}

#define LOGGER_MAKE_NEW_LOG_TYPE(type)\
	constexpr char type ##_log_name[] = #type ".log";\
	template<const char* formatstr, typename ... T>\
	void type##_log(T ... args)\
	{\
		log<type ## _log_name,formatstr>(args...);\
	}

	LOGGER_MAKE_NEW_LOG_TYPE(error)
	LOGGER_MAKE_NEW_LOG_TYPE(console)
	LOGGER_MAKE_NEW_LOG_TYPE(vulkan)
	LOGGER_MAKE_NEW_LOG_TYPE(unit_test)

	void init();

	const char text_str[] = "%s%";
}
